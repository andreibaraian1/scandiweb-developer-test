<?php
include './Controller/Product/ProductQueries.php';
header("Access-Control-Allow-Origin: https://react-scandiweb-application.herokuapp.com");
header("Access-Control-Allow-Headers: https://react-scandiweb-application.herokuapp.com");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST, DELETE, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
$id = $_GET['id'];
$Product = new ProductQueries();
$result = $Product->deleteProduct($id);
echo($id.' Deleted');

?>

