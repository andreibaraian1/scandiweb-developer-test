import styles from './Footer.module.css';
const Footer = () => {
 return (
    <footer className={styles.footer}>
    <hr />
    <p className={styles.textCenter}>Scandiweb Test asignment</p>
  </footer>
  
 )
}
export default Footer;